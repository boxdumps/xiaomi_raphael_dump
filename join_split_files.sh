#!/bin/bash

cat bootimg/04_dtbdump_Qualcomm_Technologies,_Inc._SM8150P_v2_SoC.dtb.* 2>/dev/null >> bootimg/04_dtbdump_Qualcomm_Technologies,_Inc._SM8150P_v2_SoC.dtb
rm -f bootimg/04_dtbdump_Qualcomm_Technologies,_Inc._SM8150P_v2_SoC.dtb.* 2>/dev/null
cat system/system/product/app/webview/webview.apk.* 2>/dev/null >> system/system/product/app/webview/webview.apk
rm -f system/system/product/app/webview/webview.apk.* 2>/dev/null
cat system/system/system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system/system/system_ext/priv-app/Settings/Settings.apk
rm -f system/system/system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
