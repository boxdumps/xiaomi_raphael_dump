## walleye-user 8.1.0 OPM1.171019.011 4448085 release-keys
- Manufacturer: xiaomi
- Platform: msmnile
- Codename: raphael
- Brand: Xiaomi
- Flavor: radiant_raphael-userdebug
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: eng.sherif.20211018.111233
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/walleye/walleye:8.1.0/OPM1.171019.011/4448085:user/release-keys
- OTA version: 
- Branch: walleye-user-8.1.0-OPM1.171019.011-4448085-release-keys
- Repo: xiaomi_raphael_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
